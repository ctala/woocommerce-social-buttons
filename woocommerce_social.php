<?php

/*
  Plugin Name: Woocommerce Social Product ( Pinterest + Twitter )
  Description: Add Pinterest and Twitter buttons after product summary or before product title.
  Author: Cristian Tala Sánchez
  Version: 0.1BETA
  Author URI: www.cristiantala.cl
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License or any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 

function showSocial() {
	social_inicioWidget();
	social_showPinterest();
	social_showTwitter();
	social_showFacebook();
	social_finWidget();
social_add_scripts();

}
//add_action( 'woocommerce_after_single_product_summary', 'showSocial' );


add_filter('woocommerce_product_description_heading', 'show_social_before_title');

function show_social_before_title( $heading ) {
echo social_showPinterest(true);
return $heading;
}

function social_add_scripts()
{
	//Add pinterest Script.
	wp_register_script( 'social-pinterest-script', "//assets.pinterest.com/js/pinit.js" );
	wp_enqueue_script( 'social-pinterest-script' );
	
}
add_action( 'wp_enqueue_scripts', 'social_add_scripts' ); 


function social_inicioWidget()
{
	?>
		<div id="socialWidgets" style="float: right;width: 48%; margin-bottom: 2em;margin-top: -1.5em;border:1px solid;border-radius:1px;">
	<?
}
function social_finWidget()
{
	?>
		</div>
	<?
}


function social_showPinterest($return=false)
{
	$link = '<a href="//pinterest.com/pin/create/button/" data-pin-do="buttonBookmark" ><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" /></a>';
	if($return)
		return $link;
	else
		echo $link;
}

function social_showTwitter()
{	
	?>
	<a href="https://twitter.com/share" class="twitter-share-button" data-via="naitus" data-lang="en-gb" data-related="naitus">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
	<?

}

function social_showFacebook()
{?>


<div class="fb-send" data-href="http://www.empresasctm.cl"></div>
<?
}