=== WooCommerce Chilean Peso  ===

Contributors: Cristian Tala S.



Tags: 

Requires at least: 3, 3.3+ and WooCommerce 1.4+, Chilean Peso, Pesos Chilenos, currency

Tested up to: 3.6

Stable tag: 2.3

== Description ==

This Small Plugin add The Chilean Peso currency and symbol to woocommerce.
Add the different states to Woocommerce.
Enable the paypal payment through CLP to USD conversion.


Lo que hace este plugin es facilitar la integración de los pesos Chilenos a Woocommerce, además de agregar las distintas regiones del país ( Chile obviamente ) y aceptar pagos con paypal a través de dolares. 



== Changelog ==


= 2.2 =
Agregada la posibilidad de usar paypal con woocommerce y chilean pesos.

= 2.0 =
Added the Chilean states for WooCommerce
Agregadas las Regiones de Chile a WooCommerce
